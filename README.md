Note: 
	Source Code link: bitbucket.org/pk921/restaurant
	In case you are not able to see the code: 
	- Please let me know, I will make the repository public.
	- Screen shots of the functionalities are at bitbucket.org/pk921/restaurant_screenshots
	- DataBase ER Diagram at bitbucket.org/pk921/restaurant_screenshots
-----------------------------------------------------------------------------------------------------------------------------------	
	
The sole purpose to build this project was to learn Spring MVC & Hibernate concepts.
Tools & technology used: JAVA, Spring MVC, Hibernate, JPA, JSP, JSTL, jQyery, MySQL, MySQL Workbench Model.


Functionalities:
1. Add/Delete/Modify Customer.
2. Add/Delete/Edit Customer Address
	- A Customer can have many addresses
3. Search Customer based on CustomerId, Phone Number or Email Id.
4. Take & Maintain Order.
	- Add Customer to the Order
		- Also Handles any updation of Customer and its Address, if required.
	- Add Items to the Order.
		- If the same item is being added twice, instead of creating a new row in the bill, it will increase the Item Count & Amount.
	- Give Discount.
6. Can save Multiple Orders, till its bill get Generated, in the session.
	- TODO: Instead of saving the order in session, move it to DB.
7. Generate bills.
8. Search bill
	- By Bill No.
	- By Customer, for last n days.
	
	 ___________________________________________________________________________________________
	|Note: There are many enhancements which needs to be done, but are not there yet:			|
	|	- Authorisation (Currently a user can access all the pages)								|
	|	- Securities (Cross Site Scripting attack prevention, Cross Frame Scripting, CSRF etc.)	|
	|	- Input Validations																		|
	|___________________________________________________________________________________________|